DATE=`date +%Y-%m-%d`

tar -czvf build.tar.gz build/

echo "Adding"
git add .

echo "Committing"
echo $DATE
git commit -m $DATE

echo "Pushing"
git push origin master

echo "Done"
sleep 1